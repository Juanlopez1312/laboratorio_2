import java.util.Scanner;

public class Kata1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("di la palabra que quieras invertir :");
        String PALABRANORM = scanner.nextLine();
        StringBuilder builder = new StringBuilder(PALABRANORM);
        String PALABRAINV= builder.reverse().toString();

        System.out.println( PALABRANORM + " => " + PALABRAINV);

    }
}
